/* 
 * PhyloTree.java
 *
 * Defines a phylogenetic tree, which is a strictly binary tree 
 * that represents inferred hierarchical relationships between species
 * 
 * There are weights along each edge; the weight from parent to left child
 * is the same as parent to right child.
 *
 * Students may only use functionality provided in the packages
 *     java.lang
 *     java.util 
 *     java.io
 *     
 * Use of any additional Java Class Library components is not permitted 
 * 
 * Dana Ballou
 * Data Structures
 */
import java.util.*;
import java.io.*;
import java.lang.*;




public class PhyloTree {
    private static PhyloTreeNode overallRoot;    // The actual root of the overall tree
    private int printingDepth;            // How many spaces to indent the deepest     
    Species[] speciesArray;                                     
    public double rootWeightedDepth;
    private double rootWeightedDepthPRIVATE;
    StringBuilder figBuilder = new StringBuilder();    
    

    // CONSTRUCTOR

    // PhyloTree
    // Pre-conditions:
    //        - speciesFile contains the path of a valid FASTA input file
    //        - printingDepth is a positive number
    // Post-conditions:
    //        - this.printingDepth has been set to printingDepth
    //        - A linked tree structure representing the inferred hierarchical
    //          species relationship has been created, and overallRoot points to
    //          the root of this tree
    // Notes:
    //        - A lot happens in this step!  See assignment description for details
    //          on the input format file and how to construct the tree
    //        - If you encounter a FileNotFoundException, print to standard error
    //          "Error: Unable to open file " + speciesFilename
    //          and exit with status (return code) 1
    //        - Most of this should be accomplished by calls to loadSpeciesFile and buildTree
    
    // Every species needs to be put into a species array, which will get sent to build tree at the end.
    // Build species one by one, and insert into the array. Use arraylist for size varience, then convert to array.
    public PhyloTree(String speciesFile,int printingDepth) { //DONE except exceptions       
             
           speciesArray = loadSpeciesFile(speciesFile);        
           buildTree(speciesArray);   
           this.printingDepth = printingDepth;
        return;
    }
    // ACCESSORS

    // getOverallRoot
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns the overall root
    public PhyloTreeNode getOverallRoot() { //DONE
        return this.overallRoot;
    }

    // toString 
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns a string representation of the tree
    // Notes:
    //    - See assignment description for proper format
    //    - Can be a simple wrapper around the following toString
    //    - Hint: StringBuilder is much faster than repeated concatenation
    public String toString() {
       
        String s = toString(overallRoot, 0, rootWeightedDepth);
     
        return s;
    }

    // toString 
    // Pre-conditions:
    //    - node points to the root of a tree you intend to print
    //    - weightedDepth is the sum of the edge weights from the
    //      overall root to the current root
    //    - maxDepth is the weighted depth of the overall tree
    // Post-conditions:
    //    - Returns a string representation of the tree
    // Notes:
    //    - See assignment description for proper format
    private String toString(PhyloTreeNode node, double weightedDepth, double maxDepth) {
        
        StringBuilder treeBuilder = new StringBuilder();
        
        if (!node.isLeaf()) {
        
                         
           weightedDepth = weightedDepth + node.getDistanceToChild();
           
           treeBuilder.append(toString(node.getRightChild(), weightedDepth, maxDepth));
           
           weightedDepth = weightedDepth - node.getDistanceToChild();
           
           double numPeriods = printingDepth * (weightedDepth / maxDepth); //Maybe rounding error.
           
           StringBuilder periodBuffer = new StringBuilder();
           for (int i = 0; i < numPeriods; i++) {
              periodBuffer.append(".");
           }
           
           treeBuilder.append(periodBuffer + node.toString() + "\n");
           
           weightedDepth = weightedDepth + node.getDistanceToChild();
           
           treeBuilder.append(toString(node.getLeftChild(), weightedDepth, maxDepth));
        }
        else { //At leaf.
        
           double numPeriods = printingDepth * (weightedDepth / maxDepth); //Maybe rounding error.           
           StringBuilder periodBuffer = new StringBuilder();
           for (int i = 0; i < numPeriods; i++) {
              periodBuffer.append(".");
           }              
           treeBuilder.append(periodBuffer + node.toString() + "\n");
        }                
                
        return treeBuilder.toString();
    }
    // toTreeString 
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns a string representation in tree format
    // Notes:
    //    - See assignment description for format details
    //    - Can be a simple wrapper around the following toTreeString
    public String toTreeString() {
        
        return toTreeString(overallRoot);
    }

    // toTreeString 
    // Pre-conditions:
    //    - node points to the root of a tree you intend to print
    // Post-conditions:
    //    - Returns a string representation in tree format
    // Notes:
    //    - See assignment description for proper format
    private String toTreeString(PhyloTreeNode node) {
            
        if (node.isLeaf()) {
           double distance2 = (double)Math.round(node.getParent().getDistanceToChild() * 100000) / 100000;
           figBuilder.append(node.toString() + ":" + distance2);
        }
        else {
           figBuilder.append("(");
           toTreeString(node.getRightChild());
           figBuilder.append(",");
           toTreeString(node.getLeftChild());
           if (node == overallRoot) {
              figBuilder.append("):");
           }
           else {
              double distance = (double)Math.round(node.getParent().getDistanceToChild() * 100000) / 100000;
              figBuilder.append("):" + distance);
           }
        }
        return figBuilder.toString();
    }

    // getHeight
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns the tree height as defined in class
    // Notes:
    //    - Can be a simple wrapper on nodeHeight
    public int getHeight() { //DONE
    
       int heightOfTree = nodeHeight(overallRoot); 
       return heightOfTree;
    }

    // getWeightedHeight
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns the sum of the edge weights along the
    //      "longest" (highest weight) path from the root
    //      to any leaf node.
    // Notes:
    //   - Can be a simple wrapper for weightedNodeHeight
    public double getWeightedHeight() {
        return weightedNodeHeight(overallRoot);        
    }

    // countAllSpecies
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns the number of species in the tree
    // Notes:
    //    - Non-terminals do not represent species
    //    - This functionality is provided for you elsewhere
    //      just call the appropriate method
    public int countAllSpecies() { //DONE
        return overallRoot.getNumLeafs();
    }
    // getAllSpecies
    // Pre-conditions:
    //    - None
    // Post-conditions:
    //    - Returns an ArrayList containing all species in the tree
    // Notes:
    //    - Non-terminals do not represent species
    public java.util.ArrayList<Species> getAllSpecies() { //DONE
        
        ArrayList<Species> arrayListOfSpecies = new ArrayList<Species>(Arrays.asList(speciesArray));
        
        return arrayListOfSpecies;
    }

    // findTreeNodeByLabel
    // Pre-conditions:
    //    - label is the label of a tree node you intend to find
    //    - Assumes labels are unique in the tree
    // Post-conditions:
    //    - If found: returns the PhyloTreeNode with the specified label
    //    - If not found: returns null
    public PhyloTreeNode findTreeNodeByLabel(String label) { //DONE       
        
        return findTreeNodeByLabel(overallRoot, label);
    }        
    // findLeastCommonAncestor
    // Pre-conditions:
    //    - label1 and label2 are the labels of two species in the tree
    // Post-conditions:
    //    - If either node cannot be found: returns null
    //    - If both nodes can be found: returns the PhyloTreeNode of their
    //      common ancestor with the largest depth
    //      Put another way, the least common ancestor of nodes A and B
    //      is the only node in the tree where A is in the left tree
    //      and B is in the right tree (or vice-versa)
    // Notes:
    //    - Can be a wrapper around the static findLeastCommonAncestor
     public PhyloTreeNode findLeastCommonAncestor(String label1, String label2) { //DONE
        
        PhyloTreeNode node1 = findTreeNodeByLabel(label1);
        PhyloTreeNode node2 = findTreeNodeByLabel(label2);       
        
        
        return findLeastCommonAncestor(node1, node2);            
    }
    // findEvolutionaryDistance
    // Pre-conditions:
    //    - label1 and label2 are the labels of two species in the tree
    // Post-conditions:
    //    - If either node cannot be found: returns POSITIVE_INFINITY
    //    - If both nodes can be found: returns the sum of the weights 
    //      along the paths from their least common ancestor to each of
    //      the two nodes
     public double findEvolutionaryDistance(String label1, String label2) {
        
        PhyloTreeNode node1 = findTreeNodeByLabel(label1);
        PhyloTreeNode node2 = findTreeNodeByLabel(label2);
        double distance = 0;
        
        if (node1 == null || node2 == null) {
           return Double.POSITIVE_INFINITY;
        }
        PhyloTreeNode ancestor = findLeastCommonAncestor(node1.getLabel(), node2.getLabel());             
               
        double weight1 = wHeightHelper(node1, ancestor.getLabel(), distance);        
        double weight2 = wHeightHelper(node2, ancestor.getLabel(), distance);
        double combinedWeight = weight1 + weight2;
        
        return combinedWeight;
    }
    
    public static double wHeightHelper(PhyloTreeNode node, String label, double distance) { //Helps keep track and holds the recursive call.
      
       if (!node.getLabel().equals(label)) {          
          distance = distance + node.getParent().getDistanceToChild();
          return wHeightHelper(node.getParent(), label, distance);
       }
       return distance;
   }

    // MODIFIER

    // buildTree
    // Pre-conditions:
    //    - species contains the set of species for which you want to infer
    //      a phylogenetic tree
    // Post-conditions:
    //    - A linked tree structure representing the inferred hierarchical
    //      species relationship has been created, and overallRoot points to
    //      the root of said tree
    // Notes:
    //    - A lot happens in this step!  See assignment description for details
    //      on how to construct the tree
    //    - Important hint: although the distances are defined recursively, you
    //      do not want to implement them recursively, as that would be very inefficient
    
    //private double computeTreeDistance(
    
    private void buildTree(Species[] species) { //DONE
    
        MultiKeyMap<Double> distances = new MultiKeyMap<Double>(); //All the distance between pairs in a multikey map
        HashMap<String, PhyloTreeNode> stringToTreeNode = new HashMap<String, PhyloTreeNode>(); //Put in string to get species object.
        //HashSet<String> pairsOfSpecies = new HashSet<String>(); //All possible pairs of species       
        HashSet<PhyloTreeNode> treeNodeArray = new HashSet<PhyloTreeNode>(); //A set of PTNodes (The Forest).  
        LinkedList<String> nodesWithEqualDistances = new LinkedList<String>();      
        double distanceBetweenPair;                
        double value;       
        
        // Make forest of one-node trees.
        
        for (Species s : species) {         
           
           treeNodeArray.add(new PhyloTreeNode(null, s)); // An array of all the species.
           stringToTreeNode.put(s.getName(), new PhyloTreeNode(null, s)); 
        }    
           
        for (PhyloTreeNode i : treeNodeArray) { // Finds distances of single-node trees.
           for (PhyloTreeNode j : treeNodeArray) {            
              value = i.getSpecies().distance(i.getSpecies(),j.getSpecies());
              distances.put(i.getSpecies().getName(),j.getSpecies().getName(),value); // Put in all possible distances between species.
           }
        }
        double minimumDistance = 1000; 
        PhyloTreeNode rightChild = null;
        PhyloTreeNode leftChild = null;
        PhyloTreeNode temp1 = null;
        PhyloTreeNode temp2 = null;
        Species spe1;
        Species spe2;
        String s1;
        String s2;
        String label;
        double leftLeaves;
        double rightLeaves;
        double leftAndRightLeaves;     
        double formula1;
        double formula2;
        double distanceFromTree;
        PhyloTreeNode k;
        
        while (stringToTreeNode.size() != 1) {
           
           nodesWithEqualDistances.clear();
           minimumDistance = 1000;   
                
           for (String a : stringToTreeNode.keySet()){ 
              for (String b : stringToTreeNode.keySet()) {    
               
                if (!a.equals(b)) {
                 
                    s1 = a;
                    s2 = b;
               
                    distanceBetweenPair = distances.get(s1, s2);
                    
                 
                    if (distanceBetweenPair > minimumDistance) { //changed this
                       minimumDistance = minimumDistance;                       
                                     
                    } 
                    else {
                    
                       if (distanceBetweenPair == minimumDistance) {
                          
                          
                          if (!nodesWithEqualDistances.contains(s1 + "@" + s2) && !nodesWithEqualDistances.contains(s2 + "@" + s1)) {
                             String stringToAdd = "";
                             if (s1.compareTo(s2) < 0) {
                                stringToAdd = s1 + "@" + s2;
                             }
                             else {
                                stringToAdd = s2 + "@" + s1;
                             } 
                          nodesWithEqualDistances.add(stringToAdd);
                          }
                                            
                       }
                       
                       if (distanceBetweenPair < minimumDistance) {
                          nodesWithEqualDistances.clear();
                          String stringToAdd = "";
                          
                          if (s1.compareTo(s2) < 0) {
                             stringToAdd = s1 + "@" + s2;
                          }
                          else {
                             stringToAdd = s2 + "@" + s1;
                          } 
                          nodesWithEqualDistances.add(stringToAdd);
                          
                       }
                       
                       minimumDistance = distanceBetweenPair; // Found shorter distance, update children.                       
                    }
                 }           
              }
           }
           //Get alphabetically smallest combined pair from LList.
           String smallestString = nodesWithEqualDistances.get(0);
           
           for (String compareString : nodesWithEqualDistances) {
              if (compareString.compareTo(smallestString) < 0) {
                 smallestString = compareString;
              }
           }
           
           String[] stringParts = smallestString.split("\\@");
           String leftChildString = stringParts[0];
           String rightChildString = stringParts[1];
           leftChild = stringToTreeNode.get(leftChildString);
           rightChild = stringToTreeNode.get(rightChildString);
           label = leftChild.getLabel() + "+" + rightChild.getLabel();
           overallRoot = new PhyloTreeNode (label, null, leftChild, rightChild, distances.get(leftChild.getLabel(), rightChild.getLabel())/2);
           overallRoot.getRightChild().setParent(overallRoot);
           overallRoot.getLeftChild().setParent(overallRoot);  
           
           // Add/Remove to forest.
           stringToTreeNode.remove(overallRoot.getRightChild().getLabel());
           stringToTreeNode.remove(overallRoot.getLeftChild().getLabel());
           treeNodeArray.add(overallRoot);
           stringToTreeNode.put(overallRoot.getLabel(), overallRoot);
           
           for (String s : stringToTreeNode.keySet()){
               
              if (!s.equals(overallRoot.getLabel())) { 
                  k = stringToTreeNode.get(s);
                
                    leftLeaves = overallRoot.getLeftChild().getNumLeafs();
                    rightLeaves = overallRoot.getRightChild().getNumLeafs();
                    leftAndRightLeaves = leftLeaves + rightLeaves;   
                   
                    formula1 = (leftLeaves / leftAndRightLeaves);                       
                                           
                    formula2 = (rightLeaves / leftAndRightLeaves);
                   
                    distanceFromTree = (formula1)*(distances.get(k.getLabel(), overallRoot.getLeftChild().getLabel())) + 
                                      (formula2)*(distances.get(k.getLabel(), overallRoot.getRightChild().getLabel()));
                      
                    distances.put(k.getLabel(), overallRoot.getLabel(), distanceFromTree); 
                 } 
             }
       }
          rootWeightedDepth = weightedNodeHeight(overallRoot);
          rootWeightedDepthPRIVATE = rootWeightedDepth;
          return;
      }
   
    // STATIC

    // nodeDepth
    // Pre-conditions:
    //    - node is null or the root of tree (possibly subtree)
    // Post-conditions:
    //    - If null: returns -1
    //    - Else: returns the depth of the node within the overall tree
    public static int nodeDepth(PhyloTreeNode node) { //DONE
        
        int depth = 0; 
              
        if (node == null) {
           return -1;
        }
        if (node.getParent() == null) {
           return 0;
        }
        
        return nodeDepthHelper(node.getParent(), depth);
        
    }
   public static int nodeDepthHelper(PhyloTreeNode node, int depth) { //DONE, this keeps track of the depth and calls itself recursively.
        
        depth++;
        if (node.getParent() == null) { //We are at overallRoot.
           return depth;
        }
        else {
           return nodeDepthHelper(node.getParent(), depth);
        }
        
     }

    // nodeHeight
    // Pre-conditions:
    //    - node is null or the root of tree (possibly subtree)
    // Post-conditions:
    //    - If null: returns -1
    //    - Else: returns the height subtree rooted at node
    
    public static int nodeHeight(PhyloTreeNode node) { //How can I test without a node? //DONE
    
      return nodeHeightHelper(node) - 1;
   }
   
   public static int nodeHeightHelper (PhyloTreeNode node) {  //DONE  
        
       if (node == null) {
          return 0;
       }
       else {
          return (getBiggest(nodeHeightHelper(node.getLeftChild()), nodeHeightHelper(node.getRightChild())) + 1);
       }
   }
       
    public static int getBiggest(int l, int r) { //DONE
       
       if (l >= r) {
          return l;
       }
       else {
          return r;
       }
    }

    // weightedNodeHeight 
    // Pre-conditions:
    //    - node is null or the root of tree (possibly subtree)
    // Post-conditions:
    //    - If null: returns NEGATIVE_INFINITY
    //    - Else: returns the weighted height subtree rooted at node
    //     (i.e. the sum of the largest weight path from node
    //     to a leaf; this might NOT be the same as the sum of the weights
    //     along the longest path from the node to a leaf)
    public static double weightedNodeHeight(PhyloTreeNode node) { //DONE
        
                
        HashMap<PhyloTreeNode, Double> weightedDistances = new HashMap<PhyloTreeNode, Double>();
        HashSet<PhyloTreeNode> nodeSpecies = new HashSet<PhyloTreeNode>();
        String allSpecies = node.getLabel();        
        String[] splitSpecies = allSpecies.split("\\+");
        
        for (String s : splitSpecies) {
           nodeSpecies.add(findTreeNodeByLabel(overallRoot, s));
        }
        for (PhyloTreeNode auxNode : nodeSpecies) {
            weightedDistances.put(auxNode, 0.0);
            String label = node.getLabel(); 
            wHeightHelper(auxNode, auxNode, label, weightedDistances);       
        } 
        double maxDist = 0;
        
        for (Map.Entry<PhyloTreeNode, Double> entry : weightedDistances.entrySet())  {
           PhyloTreeNode key = entry.getKey();
           double value = entry.getValue();
           
           if (value > maxDist) {
              maxDist = value;
           }
        } 
               
        return maxDist;
    }
    
    public static void wHeightHelper(PhyloTreeNode SNode, PhyloTreeNode node, String label, HashMap<PhyloTreeNode, Double> weightedDistances ) {
    
       if (!node.getLabel().equals(label)) {       
           
          weightedDistances.put(SNode, weightedDistances.get(SNode) + node.getParent().getDistanceToChild());//Adds up the distances
          wHeightHelper(SNode, node.getParent(), label, weightedDistances);
       }
       else {
          return;
      }
   }

    // loadSpeciesFile
    // Pre-conditions:
    //    - filename contains the path of a valid FASTA input file
    // Post-conditions:
    //    - Creates and returns an array of species objects representing
    //      all valid species in the input file
    // Notes:
    //    - Species without names are skipped
    //    - See assignment description for details on the FASTA format
    public static Species[] loadSpeciesFile(String filename) { //DONE
    
        ArrayList<Species> species = new ArrayList<Species>(); // Storage of variable size for species.
        String fileName; 
        String speciesName;
        Scanner reader = null;
        boolean hasName = true;
        
        try {
        reader = new Scanner(new File(filename));     
        } 
        catch (FileNotFoundException ex) {         
           System.out.println("Error: Unable to open file " + filename);
           System.exit(1);
        }      
        
        String nextLine = reader.nextLine();
         
        while (reader.hasNextLine()) {            
           String line = nextLine;
            
           if (line.contains("|")){
              StringBuilder sequence = new StringBuilder();
              String[] firstLine = line.split("\\|");// Gotta check and see if the species has a name.
              if (firstLine.length != 7) {
                 hasName = false;
              }
              speciesName = firstLine[firstLine.length - 1];
              nextLine = reader.nextLine();
               
              while (!nextLine.contains("|")) {
               
                 if (!nextLine.contains("|")) {
                    sequence.append(nextLine);
                    if (reader.hasNextLine()) {                     
                       nextLine = reader.nextLine();
                       }
                       else{
                          break; 
                    }                
                 }
               }
                 if (hasName == true) {
                    
                    String[] seqArray = new String[sequence.length()];
                    String seqString = sequence.toString();
                    for (int i = 0; i < seqArray.length; i++) {
                        seqArray[i] = sequence.substring(i, i + 1);
                    }                    
                    species.add(new Species(speciesName, seqArray)); 
                 }
                 hasName = true;
            }
         }
       Species[] speArray = species.toArray(new Species[species.size()]);
       return speArray;
    }

    // getAllDescendantSpecies
    // Pre-conditions:
    //    - node points to a node in a phylogenetic tree structure
    //    - descendants is a non-null reference variable to an empty arraylist object
    // Post-conditions:
    //    - descendants is populated with all species in the subtree rooted at node
    //      in in-/pre-/post-order (they are equivalent here)
    private static void getAllDescendantSpecies(PhyloTreeNode node,java.util.ArrayList<Species> descendants) { //DONE
        
        String allSpecies = node.getLabel();
        
        String[] splitSpecies = allSpecies.split("\\+");
        
        Species speciesToAdd;
        
        for (int i = 0; i < splitSpecies.length; i++) {           
           
           descendants.add(findTreeNodeByLabel(overallRoot, splitSpecies[i]).getSpecies());
        }   
        return;
    }

    // findTreeNodeByLabel
    // Pre-conditions:
    //    - node points to a node in a phylogenetic tree structure
    //    - label is the label of a tree node that you intend to locate
    // Post-conditions:
    //    - If no node with the label exists in the subtree, return null
    //    - Else: return the PhyloTreeNode with the specified label 
    // Notes:
    //    - Assumes labels are unique in the tree
    private static PhyloTreeNode findTreeNodeByLabel(PhyloTreeNode node,String label) { //DONE
            
            return findNodeHelper(node, label);
    }

    private static PhyloTreeNode findNodeHelper(PhyloTreeNode node, String label) {  //DONE       
               
        if (node != null) { 
           if (node.getLabel().equals(label)) { //Found it.
              return node;
           }
           else {
               PhyloTreeNode foundNode = findNodeHelper(node.getLeftChild(), label); // Search down the left side.             
               if (foundNode == null) {
                   foundNode = findNodeHelper(node.getRightChild(), label); // Search down the right side.
               }           
               return foundNode;
            }
         }
         else {
            return null; //Node is null, we reached the end without finding it.
         }
    }


    // findLeastCommonAncestor
    // Pre-conditions:
    //    - node1 and node2 point to nodes in the phylogenetic tree
    // Post-conditions:
    //    - If node1 or node2 are null, return null
    //    - Else: returns the PhyloTreeNode of their common ancestor 
    //      with the largest depth
     private static PhyloTreeNode findLeastCommonAncestor(PhyloTreeNode node1, PhyloTreeNode node2) { //DONE
        
        HashSet<PhyloTreeNode> parents = new HashSet<PhyloTreeNode>();        
        
        if (node1 == null || node2 == null) {
           return null;
        }
        return ancestorHelper(node1, node2, parents);
    }
    
    private static PhyloTreeNode ancestorHelper(PhyloTreeNode node1, PhyloTreeNode node2, HashSet<PhyloTreeNode> parents) { //DONE
       
       PhyloTreeNode commonParent1;
       PhyloTreeNode commonParent2;
       
       if (node1.getParent() != null) {
          commonParent1 = node1.getParent();
          if (parents.contains(commonParent1)) {
             return commonParent1;
          }
          parents.add(node1.getParent());
       }
       if (node2.getParent() != null) {
         commonParent2 = node2.getParent();
         if (parents.contains(commonParent2)) {
            return commonParent2;
         }
         parents.add(node2.getParent());
       } 
       
       if (node1.getParent() == null && node2.getParent() != null) {
          return ancestorHelper(node1, node2.getParent(), parents);
       }     
       if (node1.getParent() != null && node2.getParent() == null) {
          return ancestorHelper(node1.getParent(), node2, parents);
       }
       if (node1.getParent() != null && node2.getParent() != null) {
          return ancestorHelper(node1.getParent(), node2.getParent(), parents);
       }
       return null;
    }
    
    public void test(String node) {
       ArrayList<Species> testA = new ArrayList<Species>();
       getAllDescendantSpecies(findTreeNodeByLabel(node), testA);
    }
}
//THE END
