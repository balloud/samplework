import java.util.*;
import java.lang.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DatabaseMetaData;

/*
Dana Ballou
CS 330
ASSIGNMENT 3
11/12/14
*/

public class Assignment3 {

	static Connection conn = null;
    static Connection conn2 = null;
    			

	public static void main(String[] args) throws Exception{

		PrintWriter out = new PrintWriter("DATAOUTPUT.txt", "UTF-8");
        String paramsFile = "readerparams.txt";
        String paramsFile2 = "writerparams.txt";
		if (args.length >= 2){
			paramsFile = args[0];
            paramsFile2 = args[1];

		}
		Properties connectprops = new Properties();
        connectprops.load(new FileInputStream(paramsFile));
        Properties connectprops2 = new Properties();
        connectprops2.load(new FileInputStream(paramsFile2));

         try {
            // Get connection
            Class.forName("com.mysql.jdbc.Driver");
            String dburl = connectprops.getProperty("dburl");
            String dburl2 = connectprops2.getProperty("dburl");
            String username = connectprops.getProperty("user");
            String username2 = connectprops2.getProperty("user");
            conn = DriverManager.getConnection(dburl, connectprops);
            conn2 = DriverManager.getConnection(dburl2, connectprops2);
            System.out.printf("Database connection %s %s established.%n", dburl, 
            username);
            System.out.printf("Database connection %s %s established.%n", dburl2, 
            username2);
            DatabaseMetaData meta = conn2.getMetaData();
            ResultSet checkTable = meta.getTables(null, null, "Performance", null);

            if (checkTable.next()) { //Table already exists, remove it.
                Statement dt = conn2.createStatement();
                String dropTable = "DROP TABLE Performance";
                dt.execute(dropTable);
                System.out.println("Dropping previous performance table");
            }            

            Statement statement = conn2.createStatement();
            String createTable = 
                "create table Performance " +
                 "(Industry CHAR(30), " +
                 "Ticker CHAR(6), " + 
                 "StartDate CHAR(10), " + 
                 "EndDate CHAR(10), " + 
                 "TickerReturn CHAR(12), " + 
                 "IndustryReturn CHAR(12))";
            statement.execute(createTable);             


		ArrayList<String> industryList = getIndustries();

        for (String s : industryList) {

            
            System.out.println("Processing " + s);
            String[] data = getMinMaxDates(s);
            String firstTrade = data[0];
            String lastTrade = data[1];
            String firstTicker = data[2];
            int numTradeDays = getCommonDates(s, firstTrade, lastTrade, firstTicker);
            if (numTradeDays >= 150) {
                getIndustryData(s, firstTrade, lastTrade, numTradeDays, out);
                out.println("Start of interval = " + firstTrade);
                out.println("End of interval = " + lastTrade);
                out.println("Interval trades = " + numTradeDays);
                out.println("~~~~~~~Done!~~~~~~~~~~~~~");
                out.println();
            }
            else {
                out.print("Not enough data for stock " + s);
            }

        }
        test();

        conn.close();
        conn2.close();
        out.close();        

        } catch (SQLException ex) {
        System.out.printf("SQLException: %s%nSQLState: %s%nVendorError: %s%n",
            ex.getMessage(), ex.getSQLState(), ex.getErrorCode());
        }                    
		
	}

    public static int getCommonDates(String industry, String minDate, String maxDate, String firstTicker) throws SQLException{

        //System.out.println(firstTicker);
        PreparedStatement getDates = conn.prepareStatement(
            "select count(distinct TransDate) as TradingDays " +
                "from Company natural left outer join PriceVolume " +
                    "where Industry = ? and Ticker = ?" +
                        "and TransDate >= ? and TransDate <= ?");
        getDates.setString(1, industry);
        getDates.setString(2, firstTicker);
        getDates.setString(3, minDate);
        getDates.setString(4, maxDate);
        
        ResultSet cd = getDates.executeQuery();
        int comDates = 0;
        while (cd.next()) {
            comDates = Integer.parseInt(cd.getString("TradingDays"));
        }       
        getDates.close();
        return comDates;
    }

    public static ArrayList<String> getIndustries() throws SQLException {

        ArrayList<String> industryList = new ArrayList<String>();
        PreparedStatement stmt = conn.prepareStatement(
            "select Industry, count(distinct Ticker) as TickerCnt " +
                "from Company natural join PriceVolume " +
                    "group by Industry " +
                    "order by TickerCnt DESC, Industry");
        ResultSet r = stmt.executeQuery();
        

        while (r.next()) {
            //System.out.println(r.getString("Industry") + r.getString("TickerCnt"));
            industryList.add(r.getString("Industry"));
        }
        stmt.close();
        return industryList;
    }

    public static String[] getMinMaxDates(String industry) throws SQLException {
        
        ArrayList<String> mins = new ArrayList<String>();
        ArrayList<String> maxs = new ArrayList<String>();
        String largest = "0000.00.00";
        String smallest = "9999.99.99";
        PreparedStatement stmt2 = conn.prepareStatement(
            "select Industry, Ticker, min(TransDate) as minDate, max(TransDate) as maxDate, count(distinct TransDate) as TradingDays " +
                "from Company natural join PriceVolume " +
                    "where Industry = ? " + //and TransDate >= 'max(min(TransDate))' and TransDate <= 'min(max(TransDate))' " + 
                    "group by Ticker " +
                    "having TradingDays >= 150 " +
                    "order by Ticker");
        stmt2.setString(1, industry); //Puts in industry passed in, uses value for industry in SQL query.
        ResultSet r2 = stmt2.executeQuery();      

        int y = 1; 
        String firstTicker = " ";
        while (r2.next()) {
            if (y == 1) {
                firstTicker = r2.getString("Ticker");
                y = 0;
            }            
            mins.add((r2.getString("minDate")));
            maxs.add((r2.getString("maxDate")));
            


        }

        for (String minDate : mins) {
            if (minDate.compareTo(largest) >= 0) {
                largest = minDate;
            }  
        }

        for (String maxDate : maxs) {
            if (maxDate.compareTo(smallest) <= 0) {
                smallest = maxDate;
            }
        }
        String firstDate = largest;
        String lastDate = smallest;        
        String resultArray[] = new String[] {firstDate, lastDate, firstTicker};  
        stmt2.close();
        return resultArray;
    }    

    public static void getIndustryData(String industry, String minDate, String maxDate, int tradeDays, PrintWriter out) throws SQLException, IOException {       
        
        
        out.println("~~~~~~~~~~~~~~Processing " + industry + "~~~~~~~~~~~~~~~~~");
        PrintWriter i = new PrintWriter("i.txt", "UTF-8");        
        ArrayList<String> tickerList = new ArrayList<String>();        
        double divisor = 1;

        PreparedStatement stmt4 = conn.prepareStatement(
            "select distinct Ticker, count(distinct TransDate) as TradingDays " +
                "from Company natural join PriceVolume " +
                    "where Industry = ? " + 
                    "group by Ticker having TradingDays >= 150 " +                    
                    "order by Ticker");
        stmt4.setString(1, industry);
        ResultSet r4 = stmt4.executeQuery();
        while (r4.next()) {
            tickerList.add(r4.getString("Ticker")); 
        }
        stmt4.close();        

        PreparedStatement stmt3 = conn.prepareStatement(
                "select Ticker, TransDate, openPrice, closePrice " +                
                "  from PriceVolume natural join Company " +
                "  where Industry = ? and TransDate >= ? and TransDate <= ? " +
                "  order by TransDate, Ticker ");

        stmt3.setString(1, industry);
        stmt3.setString(2, minDate);
        stmt3.setString(3, maxDate);
        ResultSet r3 = stmt3.executeQuery(); 

        PreparedStatement stmtr = conn.prepareStatement(
                "select Ticker, TransDate, openPrice, closePrice " +                
                "  from PriceVolume natural join Company " +
                "  where Industry = ? and TransDate >= ? and TransDate <= ? " +
                "  order by TransDate, Ticker ");

        stmtr.setString(1, industry);
        stmtr.setString(2, minDate);
        stmtr.setString(3, maxDate); 
        ResultSet intervalR = stmtr.executeQuery();

        ArrayList<String> intervalDates = getIntervalDates(intervalR);
        String FIRSTTICKER = intervalDates.get(0);
        intervalDates.remove(0);
        if ((intervalDates.size() & 1) != 0) {//size is odd.
            intervalDates.remove(intervalDates.size()-1);
        }   
        Map<Integer, String> dateMap = new HashMap<Integer, String>(); 
        int load = 1;    
        for (String s : intervalDates) {
            dateMap.put(load, s);
            i.println(load + " interval date is : " + s);
            load++;
        }        
        
        int numIntervals = (tradeDays / 60);
        int NUMBEROFTICKERS = tickerList.size();
        System.out.println(NUMBEROFTICKERS);
        out.println(numIntervals);
        double numberOfShares;       

        out.println("^^^^^^^^" + NUMBEROFTICKERS + "^^^^^^^^^^");  
        
        int intervalIndex = 1;       
        String currentInterval = dateMap.get(intervalIndex);
        int CI = 1;
        boolean opening = true;
        boolean closing = false;   
        boolean increment = false;  
        ArrayList<Interval> intervalBucket = new ArrayList<Interval>();
        Interval intervalObject = new Interval("0", "0");
        while (r3.next()) {            
            
            while (r3.getString("Transdate").equals(currentInterval)) { //First day of interval.                
                
                if (opening == true){                    
                    out.println("First Day of interval " + (CI) + " : " + (r3.getString("Ticker") + " " + r3.getString("TransDate") + " " + r3.getString("openPrice") + " " + r3.getString("closePrice")));
                    intervalObject.addToS(r3.getString("Ticker"), Double.parseDouble(r3.getString("openPrice")));
                    intervalObject.SSD(r3.getString("Transdate"));
                }
                if (closing == true) {                    
                    out.println("Last Day of interval " + (CI) +" : " +(r3.getString("Ticker") + " " + r3.getString("TransDate") + " " + r3.getString("openPrice") + " " + r3.getString("closePrice")));
                    intervalObject.addToE(r3.getString("Ticker"), Double.parseDouble(r3.getString("closePrice")));
                    intervalObject.SED(r3.getString("TransDate"));                
                }
                r3.next();
                increment = true;
            }            
            if (increment == true) {
                out.println();
                intervalIndex++;
                currentInterval = dateMap.get(intervalIndex); 
                
                if(r3.getString("Transdate").equals(currentInterval)) {
                    intervalBucket.add(intervalObject);
                    intervalObject = new Interval("0", "0"); 
                    CI++;
                    out.println("First Day of interval " + (CI) + " : " + (r3.getString("Ticker") + " " + r3.getString("TransDate") + " " + r3.getString("openPrice") + " " + r3.getString("closePrice")));
                    intervalObject.addToS(r3.getString("Ticker"), Double.parseDouble(r3.getString("openPrice")));
                    //change intervals
                }                
                increment = false;
                opening = !opening;
                closing = !closing;
            }          

        }
        intervalBucket.add(intervalObject);
        stmt3.close();
        i.close();
        compute(industry, intervalBucket, tickerList);
    }
  
        public static ArrayList<String> getIntervalDates(ResultSet r) throws SQLException, IOException {
            ArrayList<String> list = new ArrayList<String>();
            PrintWriter w = new PrintWriter("w.txt", "UTF-8");
            String FIRSTTICKER = " ";
            int i = 0;
            int count = 0;            

            while (r.next()) {

                if (i == 0) {
                    FIRSTTICKER = r.getString("Ticker");
                    list.add(FIRSTTICKER);
                    list.add(r.getString("TransDate")); 
                    w.println("1st Start date : " + r.getString("Ticker") + r.getString("TransDate"));
                    i++;                 
                }
                if (r.getString("Ticker").equals(FIRSTTICKER)) {
                    count ++;                    

                    if (count == 60) {
                        list.add(r.getString("Transdate"));
                        w.println("End date   " + r.getString("Ticker") + r.getString("TransDate"));
                        w.print("-----------------------");
                        w.println();                        
                    }
                    if (count == 61) {
                        list.add(r.getString("Transdate"));
                        w.println("Start date   " + r.getString("Ticker") + r.getString("TransDate"));
                        w.println();
                        count = 1;
                    }   
                }
            } 
        w.close(); 
        return list;
    }    

    public static void compute(String industry, ArrayList<Interval> list, ArrayList<String> tickerList) throws SQLException, IOException {

        HashMap<String, Double> tickerReturns = new HashMap<String, Double> ();
        HashMap<String, Double> industryReturns = new HashMap<String, Double> ();
        double tickerReturn = 0;
        double industryReturn = 0;
        PrintWriter p = new PrintWriter("p.txt", "UTF-8");
        double sum = 0;

        for (Interval i : list) {

            industryReturn = 0;
            tickerReturn = 0;
            tickerReturns = new HashMap<String, Double> ();
            industryReturns = new HashMap<String, Double> ();
            p.println("##############################");
            p.println(i.startDate);

            for (String t1 : tickerList) {
                tickerReturn = ((i.eMap.get(t1)) / (i.sMap.get(t1)))-1;
                tickerReturns.put(t1, (tickerReturn));   
            }

            for (String t1 : tickerList) {
                sum = 0;
                for (String t2 : tickerList) {
                    if (!t2.equals(t1)) {
                        sum += (tickerReturns.get(t2));
                    }
                }
                industryReturn = (((1.0/(double)(tickerList.size() - 1.0)) * (sum)));  
                industryReturns.put(t1, industryReturn);       
            }
            for (String k : tickerList) {
                p.println(k + "  " +  i.startDate + "  " + i.endDate + "  " + tickerReturns.get(k) + "  " + industryReturns.get(k));
                printToPerformance(industry, k, i.startDate, i.endDate, tickerReturns.get(k), industryReturns.get(k));
            }          
      }
    p.close();
    }    

    public static void printToPerformance(String industry, String ticker, String startDate, String endDate, double tickerReturn, double industryReturn) throws SQLException {
        
        PreparedStatement updateTable = conn2.prepareStatement(
                "insert into Performance " +
                                "VALUES (?, ?, ?, ?, ?, ?)");
        updateTable.setString(1, industry);
        updateTable.setString(2, ticker);
        updateTable.setString(3, startDate);
        updateTable.setString(4, endDate);
        updateTable.setString(5, String.format("%10.7f", tickerReturn));
        updateTable.setString(6, String.format("%10.7f", industryReturn));
        updateTable.execute();
        updateTable.close();

    }

    public static void test() throws SQLException, IOException {
        PrintWriter t = new PrintWriter("t.txt", "UTF-8");
        PreparedStatement test = conn2.prepareStatement(
            "select * from Performance order by StartDate, Ticker");
    ResultSet ts = test.executeQuery();
    while (ts.next()) {
        t.println(ts.getString("Industry") + "  | " + ts.getString("Ticker") + "  | " + ts.getString("StartDate") + "  | " + ts.getString("EndDate") + "  | " + ts.getString("TickerReturn") + "  | " + ts.getString("IndustryReturn"));
    }
    t.close();
    test.close();


    }
    


    


     





    

}
