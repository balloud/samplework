import java.util.*;
import java.lang.*;

public class Day {

	public String ticker;
	public String date;
	public double openPrice;
	public double closePrice;
	 


	public Day(String ticker, String date, double openPrice, double closePrice) {
		this.ticker = ticker;
		this.date = date;
		this.openPrice = openPrice;
		this.closePrice = closePrice;		
	}

	public Day(String date, double openPrice, double closePrice, double avg) {
		this.date = date;
		this.openPrice = openPrice;
		this.closePrice = closePrice;		
	}

	public Day() {
		this.date = "0000";
		this.openPrice = 0.00;
		this.closePrice = 0.00;
	}

}