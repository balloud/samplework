/* 
 * CS:APP Data Lab 
 * 
 * <Dana Ballou, these are my solutions to the bits projects>
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */

#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

INTEGER CODING RULES:
 
  Replace the "return" statement in each function with one
  or more lines of C code that implements the function. Your code 
  must conform to the following style:
 
  int Funct(arg1, arg2, ...) {
      /* brief description of how your implementation works */
      int var1 = Expr1;
      ...
      int varM = ExprM;

      varJ = ExprJ;
      ...
      varN = ExprN;
      return ExprR;
  }

  Each "Expr" is an expression using ONLY the following:
  1. Integer constants 0 through 255 (0xFF), inclusive. You are
      not allowed to use big constants such as 0xffffffff.
  2. Function arguments and local variables (no global variables).
  3. Unary integer operations ! ~
  4. Binary integer operations & ^ | + << >>
    
  Some of the problems restrict the set of allowed operators even further.
  Each "Expr" may consist of multiple operators. You are not restricted to
  one operator per line.

  You are expressly forbidden to:
  1. Use any control constructs such as if, do, while, for, switch, etc.
  2. Define or use any macros.
  3. Define any additional functions in this file.
  4. Call any functions.
  5. Use any other operations, such as &&, ||, -, or ?:
  6. Use any form of casting.
  7. Use any data type other than int.  This implies that you
     cannot use arrays, structs, or unions.

 
  You may assume that your machine:
  1. Uses 2s complement, 32-bit representations of integers.
  2. Performs right shifts arithmetically.
  3. Has unpredictable behavior when shifting an integer by more
     than the word size.

EXAMPLES OF ACCEPTABLE CODING STYLE:
  /*
   * pow2plus1 - returns 2^x + 1, where 0 <= x <= 31
   */
  int pow2plus1(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     return (1 << x) + 1;
  }

  /*
   * pow2plus4 - returns 2^x + 4, where 0 <= x <= 31
   */
  int pow2plus4(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     int result = (1 << x);
     result += 4;
     return result;
  }

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
/* 
 * bitAnd - x&y using only ~ and | 
 *   Example: bitAnd(6, 5) = 4
 *   Legal ops: ~ |
 *   Max ops: 8
 *   Rating: 1
 */
int bitAnd(int x, int y) {
  /*
  We take advantage of DeMorgans Law!
  */

  return ~(~x | ~y);
}
/* 
 * getByte - Extract byte n from word x
 *   Bytes numbered from 0 (LSB) to 3 (MSB)
 *   Examples: getByte(0x12345678,1) = 0x56
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 6
 *   Rating: 2
 */
int getByte(int x, int n) {
  /*
   Here, we shift 'n' left to get n*8, which is
   the number of bytes we need to move over
   to get out byts of interest. This will
   work in every case
   */
  return (0xFF & (x >> (n << 3)));
}
/* 
 * logicalShift - shift x to the right by n, using a logical shift
 *   Can assume that 0 <= n <= 31
 *   Examples: logicalShift(0x87654321,4) = 0x08765432
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 20
 *   Rating: 3 
 */
int logicalShift(int x, int n) {
  /*
  Here, we remove the sign from x,
  then shift it over, so we don't risk
  dragging along any 1's from the arithmetic shift.
  After we have shifted,
  we add the original '1' back on in its proper place,
  by shifting it over 'n' amount, and 'XOR'ing it on!
  */

  int sign = ((0x01) & (x >> 31));
  int xRemovedSign = (x & ~(0x80 << 24));
  int shiftedx = xRemovedSign >> n;
  sign = sign << (31 + (~n) + 1);

  return shiftedx ^ sign;
}
/*
 * bitCount - returns count of number of 1's in word
 *   Examples: bitCount(5) = 2, bitCount(7) = 3
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 40
 *   Rating: 4
 */
int bitCount(int x) {
  /* 
  I brute-forced this one
  just incremented 't' every time i found a bit turned on
  So effiecient ;)
  */
  int t = 0x00;
  t = t + (0x01 & x);
  t = t + (0x01 & (x >> 1));
  t = t + (0x01 & (x >> 2));
  t = t + (0x01 & (x >> 3));
  t = t + (0x01 & (x >> 4));
  t = t + (0x01 & (x >> 5));
  t = t + (0x01 & (x >> 6));
  t = t + (0x01 & (x >> 7));
  t = t + (0x01 & (x >> 8));
  t = t + (0x01 & (x >> 9));
  t = t + (0x01 & (x >> 10));
  t = t + (0x01 & (x >> 11));
  t = t + (0x01 & (x >> 12));
  t = t + (0x01 & (x >> 13));
  t = t + (0x01 & (x >> 14));
  t = t + (0x01 & (x >> 15));
  t = t + (0x01 & (x >> 16));
  t = t + (0x01 & (x >> 17));
  t = t + (0x01 & (x >> 18));
  t = t + (0x01 & (x >> 19));
  t = t + (0x01 & (x >> 20));
  t = t + (0x01 & (x >> 21));
  t = t + (0x01 & (x >> 22));
  t = t + (0x01 & (x >> 23));
  t = t + (0x01 & (x >> 24));
  t = t + (0x01 & (x >> 25));
  t = t + (0x01 & (x >> 26));
  t = t + (0x01 & (x >> 27));
  t = t + (0x01 & (x >> 28));
  t = t + (0x01 & (x >> 29));
  t = t + (0x01 & (x >> 30));
  t = t + (0x01 & (x >> 31));

  return t; 
}
/* 
 * bang - Compute !x without using !
 *   Examples: bang(3) = 0, bang(0) = 1
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 12
 *   Rating: 4 
 */
int bang(int x) {
  /*
  For bang, we mask the compliment of the shifted sign bit,
  and mask it with the compliment of -x shifted. Because the 
  2's compliment of zero is zero, we know the compliment of
  xShifted masked with the compliment of negXShifted will
  give us the correct value. We mask with 0x01 to eliminate 
  bits that we don't need to look at. 
  */

  int xShifted = (x >> 31);
  int negXShifted = ((~ x + 1) >> 31);
  
  return (0x01 & ~xShifted & ~negXShifted);
}
/* 
 * tmin - return minimum two's complement integer 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 4
 *   Rating: 1
 */
int tmin(void) {
  /* 
  This is simply the smallest value representable.
  1 followed by all zeroes simply rolls back to itself when the 
  2's compliment is taken! 
  */
  return (1 << 31);
}
/* 
 * fitsBits - return 1 if x can be represented as an 
 *  n-bit, two's complement integer.
 *   1 <= n <= 32
 *   Examples: fitsBits(5,3) = 0, fitsBits(-4,3) = 1
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int fitsBits(int x, int n) {  
  /* 
  For fitsBits, we take the number, and slide it left, leaving 'n' number
  of the original bits left. The res of the preceding data gets pushed
  "off the edge". After this, we slide it back to the right the same amount,
  so it is in it's original position. We check to see if the number equals the original
  value, and return 1 if it does. If the number is to big to be represented in 'n' bits,
  we lose data in he left shift, and the number is no longer equal to 'x'.
  */ 
 
  int distanceToShift = (32 + ((~n) + 1));
  int originalX = x;
  int xSlidLeftThenRight = (x << distanceToShift) >> distanceToShift;

  return !(originalX ^ xSlidLeftThenRight);
}
/* 
 * divpwr2 - Compute x/(2^n), for 0 <= n <= 30
 *  Round toward zero
 *   Examples: divpwr2(15,1) = 7, divpwr2(-33,4) = -2
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int divpwr2(int x, int n) {
    
  /* 
  We need to return x >> n f x is positive, 
  but we need to do more work if x is negative. We make a varibale 'correction' that will
  equal 0 if x is positive. If x is negative, 'correction' will give us a one that we can add on at the end
  to deal with rounding towards zero.
  */
  int signMask = x >> 31;    
  int allOnes = (0x01 << 31) >> 31;
  int correction = (signMask & (((0x01 << n)) + allOnes));
  
  return (x + correction) >> n;
}
/* 
 * negate - return -x 
 *   Example: negate(1) = -1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 5
 *   Rating: 2
 */
int negate(int x) {
  /*
  Simple 2's compliment here.
  Add '1' to the compliment of x.
  */

  return ((~x) + 0x01);
}
/* 
 * isPositive - return 1 if x > 0, return 0 otherwise 
 *   Example: isPositive(-1) = 0.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 3
 */
int isPositive(int x) {
  /* we check the sign bit, and if x == 0, then negate the 'OR'd value to get the answer we want*/
  return !((0x01 & (x >> 31)) | !(x));
}
/* 
 * isLessOrEqual - if x <= y  then return 1, else return 0 
 *   Example: isLessOrEqual(4,5) = 1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 24
 *   Rating: 3
 */
int isLessOrEqual(int x, int y) {

  /* 
  Because comparing negative and positive values can cause weird overflow errors,
  we check if the signs match. We do a seperate computation if the signs ARE matching (this
  is the easy case, just subtract and check the sign of the result), but if they don't match,
  we can determine the answer by looking the sign of x (since
  we know y obviously has the opposite sign.) We OR the seperate cases together to get
  out final asnwer. 
  */

  int signOfx = 0x01 & (x >> 31);
  int signOfy = 0x01 & (y >> 31);
  int matchingSignMask = (signOfx ^ signOfy); //Return 1...1 if signs don't match.  
  int ifMatching = (0x01 & ((y + (~ x + 1)) >> 31));   
  int result = (matchingSignMask & !signOfy) | (~matchingSignMask & !ifMatching);
  return result;
}
/*
 * ilog2 - return floor(log base 2 of x), where x > 0
 *   Example: ilog2(16) = 4
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 90
 *   Rating: 4
 */

/* 
 * float_neg - Return bit-level equivalent of expression -f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 *   When argument is NaN, return argument.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */
unsigned float_neg(unsigned uf) {
  /* 
  (1) For float_neg, we first obtain the exponent, which is 8-bits. We grab this data
  by shifting right 23 places so that the exponent is the rightmost 8-bits, and we mask this with 0xFF
  so that we don't grab a 1 from the sign bit if the number is negative.
  (2) We then check if the exponent is all 1's (and therefore denormalized), and also
  if the decimal is all zeroes. If both these cases are true, we leave uf alone.
  (3) If the nu,ber is not denormalized, we simply XOR it with 0x0000000 to switch the sign bit!
  */

  //1
  int ufExp = ((uf >> 23) & (0x000000FF));
  //2
  if ((ufExp == 0x000000FF) && ((uf << 9) != 0x00000000)) {
    uf = uf;
  }
    //3
    else {  
      return (uf ^ (0x80000000));
  }
  return uf;
  
}


/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {
  //Not happening . . .
  return 2;

}
/* 
 * float_twice - Return bit-level equivalent of expression 2*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_twice(unsigned uf) {
  //Also not happening . . .
  return 2;
}
