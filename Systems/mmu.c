#include "mmu.h"
#include <stdio.h>
/*
Dana Ballou
CS 247

/* (FROM PROFESSOR)
 * I'm defining the CR3 "register" as a void*, but you are free to redefine it
 * as necessary.  Keep in mind that whatever type you use, it MUST be a
 * pointer.  However, the linker will not verify this.  If you redefine this
 * global as some non-pointer type, your program will probably crash.
 */
extern unsigned* CR3; /*redefined to unsigned from void*/

/* The supervisor mode indicates that the processor is running in the operating
 * system. If the permission bits are used, e.g., not in legacy mode, then
 * accessing a privileged page is only valid when SUPER is true.  Otherwise, it
 * is a protection fault.
 */
extern int  SUPER;

/* These are our helper functions. 
   I put all shifting/masks/casts/etc.. in helper functions
   to keep the methods cleaner.
*/
static unsigned short va_PTEIndex (unsigned short va) {
	return ((va >> 8) & (0xFF));
}
static unsigned int getPTE (unsigned short va) {
	return CR3[va_PTEIndex(va)];
}
static unsigned int va_PO(unsigned short va) {
	return (va & 0xFF);
}
static unsigned int legacyValid (unsigned int pte) {
	return ((pte >> 31) & (0x1));
}
static unsigned int legacyPPN (unsigned int pte) {
	return ((pte >> 4) & (0xFFFFFF));
}
static void* getPhysicalAdress(unsigned int PPN, unsigned int PO) {	
	return (void *)((PPN << 8) ^ (PO));
}
static unsigned int getRootIndex(unsigned int address){
  return ((address >> 24) & (0xFF));
}
static unsigned int getDirectoryIndex(unsigned int address){
  return ((address >> 16) & (0xFF));
}
static unsigned int getPTEIndex(unsigned int address){
  return ((address >> 8) & (0xFF));
}
static unsigned int getPageOffset(unsigned int address){
  return (address & (0xFF));
}
static unsigned int getTLBTag(unsigned int address) {
  return ((address >> 16) & (0xFFFF));
}
static unsigned int getTLBIndex(unsigned int address) {
  return ((address >> 8) & (0xFF));
}
static unsigned int checkValid(unsigned int x) {
  return (x & 0x01);
}
static unsigned int getAddressFromDirectory (unsigned int entry) {
  return ((entry >> 4) & (0xFFFFFFFF));
}
static void* pointer_address(unsigned p) {
  return (void*)(p & ( ~1));
}
/*Here are our structure definitions*/
static result_t success (void* physicalAddress) {
	result_t res;
	res.status = SUCCESS;
	res.value.pa = physicalAddress;
	return res;
}
static result_t pageFault (unsigned VPN) {
	result_t res;
	res.status = PAGEFAULT;
	res.value.vpn = VPN;
	return res;
}
static result_t protfault (unsigned PTE) {
  result_t res;
  res.status = PROTFAULT;
  res.value.pte = PTE;
  return res;
}
/*Here is 16-bit address resolution*/
result_t mmu_legacy(unsigned short va) {
  
  result_t result;
  unsigned int PTE = getPTE(va);
  unsigned int isValid = legacyValid(PTE);

  if (isValid) {
  	unsigned int PPN = legacyPPN(PTE);
  	unsigned int PO = va_PO(va);
  	void* physicalAddress = getPhysicalAdress(PPN, PO);
  	result = success(physicalAddress);
  }
  else {  	
  	unsigned short VPN = va_PTEIndex(va);
  	result = pageFault(VPN);
  }  
  return result;
}
/*This checks for invalid permissions, and returns true
  of the permission bits don't match the intended 'access_t use'*/
static unsigned int invalidPermissions (unsigned int PTE, access_t use) {

  unsigned int P = (PTE >> 3) & 0x1;
  unsigned int R = (PTE >> 2) & 0x1;
  unsigned int W = (PTE >> 1) & 0x1;
  unsigned int X = PTE & 0x1;

  if ((SUPER == 0) && (P == 1)) {
    return 1;
  }
  if ((use == EXECUTE) && (X == 0)) {
    return 1;
  }
  if ((use == WRITE) && (W == 0)) {
    return 1;
  }
  if ((use == READ) && (R == 0)) {
    return 1;
  }
  return 0;
}
/*Here is 32-bit address resolution*/
result_t mmu_resolve(void* va, access_t use) {  
  
  unsigned int vaINT = (unsigned int) va;
  result_t result;

  /* Dissasemble virtual address*/
  unsigned int rootIndex = getRootIndex(vaINT);
  unsigned int directoryIndex = getDirectoryIndex(vaINT);
  unsigned int PTEIndex = getPTEIndex(vaINT);
  unsigned int PTE;
  unsigned int pageOffset = getPageOffset(vaINT);
  unsigned int VPN_32Bit = (vaINT >> 8) & (0xFFFFFF);
  unsigned int TLBTag = getTLBTag(vaINT);
  unsigned int TLBIndex = getTLBIndex(vaINT);

  if (tlb_search(TLBIndex, TLBTag, &PTE)) {
    /*We've found PTE*/ 
    
    if (invalidPermissions(PTE, use)) {
      return protfault(PTE);
    }
    else {
      return success((void*) getPhysicalAdress(legacyPPN(PTE), pageOffset));

    }
  }else {   
    /*Didn't find PTE*/

    unsigned int rootDirectoryEntry = CR3[rootIndex];  

    if (!checkValid(rootDirectoryEntry)) {
      return pageFault(VPN_32Bit); 
    }

    unsigned* rootDirectoryEntryPointer = pointer_address(rootDirectoryEntry); 
    unsigned int directoryEntry = rootDirectoryEntryPointer[directoryIndex];

    if (!checkValid(directoryEntry)) {
      return pageFault(VPN_32Bit);
    }  

    unsigned* directoryEntryPointer = pointer_address(directoryEntry); 
    PTE = directoryEntryPointer[PTEIndex];

    if (!legacyValid(PTE)) {
      return pageFault(VPN_32Bit);
    }
    
    unsigned int PPN_32Bit = legacyPPN(PTE); 
    void* physicalAddress_32bit = getPhysicalAdress(PPN_32Bit, pageOffset);

    tlb_add(TLBIndex, TLBTag, PTE);

    if (invalidPermissions(PTE, use)) {
      return protfault(PTE);
    }
    else {
      return success(physicalAddress_32bit);
    }
  }
}


